from django.contrib import admin

# Register your models here.
from .models import Profile

#displays profile table on admin interface
admin.site.register(Profile)