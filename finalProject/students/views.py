from django.shortcuts import render, redirect

# Create your views here.
from django.contrib.auth.decorators import login_required

from .forms import UserUpdateForm, UiUpdateForm


#displays profile edit page and enables for user update form to be used
@login_required
def profile(request):
	if request.method == 'POST':
		u_form = UserUpdateForm(request.POST,  instance= request.user)
		
		if u_form.is_valid():#checks if form is valid and saves changes
			u_form.save()
			return redirect('profile')
	else:
		u_form = UserUpdateForm(instance= request.user)

	context = {
		'u_form':u_form,
	}
	return render(request, 'students/profile.html', context)#change to /home to redirect to home page when using with base.html form



#enables user to edit ui (not a page to be navigated to) 
@login_required
def ui_edit(request):
	if request.method == 'POST':
		ui_form = UiUpdateForm(request.POST,  instance= request.user.profile)
		
		if ui_form.is_valid():#checks if form is valid and saves changes
			ui_form.save()
			return redirect(request.META.get('HTTP_REFERER', 'redirect_if_referer_not_found'))
	else:
		ui_form = UiUpdateForm(instance= request.user.profile)

	context = {
		'ui_form':ui_form,
	}
	return render(request, 'students/profile.html', context)#change to /home to redirect to home page when using with base.html form

