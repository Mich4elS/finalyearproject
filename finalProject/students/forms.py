from django import forms
from django.contrib.auth.models import User
from .models import Profile


#user update from for updating user login details

class UserUpdateForm(forms.ModelForm):
	email = forms.EmailField()#add widget=forms.TextInput(attrs={'size':15}) to params for change of widht if not using crispy
	username = forms.CharField(widget=forms.TextInput(attrs={'size':15}))#remove if needed / removes small text 

	class Meta:
		model = User
		fields = ['username', 'email']

#ui update from enables ui changes
class UiUpdateForm(forms.ModelForm):
	CHOICES=[('0','Default'),('1','1'),('2','2'),('3','3'),('4','4')]
	theme = forms.ChoiceField(choices=CHOICES,widget=forms.Select())

	class Meta:
		model = Profile
		fields = ['Fontsize','inverse','theme','layout']



