# Generated by Django 3.0.4 on 2020-06-22 20:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('students', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='Fontsize',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='profile',
            name='background',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='profile',
            name='editable',
            field=models.BooleanField(default=True),
        ),
        migrations.AddField(
            model_name='profile',
            name='inverse',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='profile',
            name='layout',
            field=models.IntegerField(default=0),
        ),
    ]
