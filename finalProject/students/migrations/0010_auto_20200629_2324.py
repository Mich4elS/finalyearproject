# Generated by Django 3.0.4 on 2020-06-29 23:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('students', '0009_auto_20200629_2252'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='Fontsize',
            field=models.CharField(choices=[('16', 'Small'), ('18', 'Medium'), ('20', 'Large')], default='Medium', max_length=3),
        ),
    ]
