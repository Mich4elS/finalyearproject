from django.db import models
from django.contrib.auth.models import User
# Create your models here.
from django.core.validators import MaxValueValidator, MinValueValidator


#choices provided by ui form
size_choices=(('14','Default'),('16','Small'),('18','Medium'),('20','Large'))

layout=(('0','0'),('1','1'),('2','2'))

#profile table for storing user and user ui data/preferences
class Profile(models.Model):
	user = models.OneToOneField(User, on_delete=models.CASCADE)
	editable = models.BooleanField(default = True)
	Fontsize = models.CharField(max_length=3, choices=size_choices, default='Default')
	inverse = models.BooleanField(default=False)
	theme = models.PositiveIntegerField(default=0,validators=[MinValueValidator(0), MaxValueValidator(5)])#change to theme
	layout = models.CharField(max_length=1, choices=layout, default='0')
	

	def __str__(self):
		return self.user.first_name