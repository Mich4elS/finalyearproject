from django.urls import path
from . import views

# main portal ui urls
urlpatterns = [
    path('', views.Home, name='portal-home'),
    path('Coursework/', views.Coursework, name='courseworks'),
    path('Modules/', views.Modules, name='modules'),
    path('textRead/', views.TextRead, name='textRead'),
    path('instructions/', views.Instructions, name='instructions'),
]
