from django.shortcuts import render, redirect
from students.forms import UserUpdateForm, UiUpdateForm

# custom context processor enables for global use of UiEditForm
def ui_edit(request):

	if request.method == 'POST':
		ui_form = UiUpdateForm(request.POST)
		if ui_form.is_valid():
			ui_form.save()
			return redirect(request.META.get('HTTP_REFERER', 'redirect_if_referer_not_found'))
	if request.user.is_authenticated:
		ui_form = UiUpdateForm(instance= request.user.profile)
	else:
		ui_form = UiUpdateForm()



	return {'ui_form':ui_form}