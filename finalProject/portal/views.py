from django.shortcuts import render

from django.contrib.auth.decorators import login_required


# Create your views here.
@login_required
def Home(request):#home view
    return render(request, 'portal/home.html')

@login_required
def Coursework(request):#coursework view

	return render(request, 'portal/coursework.html')

@login_required
def Modules(request):#module view
	return render(request, 'portal/modules.html')

@login_required
def Instructions(request):#instructions view
	return render(request, 'portal/instructions.html')

@login_required
def TextRead(request):#text reader view
	return render(request, 'portal/textRead.html')